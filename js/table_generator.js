//Farhan khandaker 2135266

"use strict";

window.addEventListener("DOMContentLoaded", intialise);

//Initialises javascript code. Creates event listeners and generates default table.
function intialise(){
    
    for(let i = 0; i < getTags("input").length; i++){
    getTags("input")[i].addEventListener("input", generateTable);
    }
    generateTable();

};

//Generates the table based on default values or regenerates based on user inputs.
function generateTable(){
    if(getTags("table").length == 1){
        getTags("table")[0].remove();
    }

    let table_css = createTag("link");
    table_css.rel = "stylesheet";
    table_css.href = "../css/table.css";
    getTags("head")[0].appendChild(table_css);

    getId("table_render_space").appendChild(createTag("table"));
    let table = getTags("table")[0];
    table.style.width = getId("table_width").value+"%";
    table.style.backgroundColor = getId("background_colour").value;
    
    let rows = getId("row_count").value;
    let columns = getId("column_count").value;
    
    let current_row;
    let current_td;

    for(let i = 1; i <= rows; i++ ){
        table.appendChild(createTag("tr"));
        current_row = getTags("tr")[i-1];
        for(let j = 1; j <= columns; j++){
            current_row.appendChild(createTag("td"));
            current_td  = current_row.getElementsByTagName("td")[j-1];
            current_td.innerText = `Cell (${i}, ${j})`;
            current_td.style.color = getId("text_colour").value;
            current_td.style.border = `${getId("border_width").value}px solid ${getId("border_colour").value}`;
        }
    }

    generateHTML(rows, columns);

};

//Generates HTML code for the default table or regenerates based on user inputs.
function generateHTML(rows, columns){
    let textarea = getTags("textarea")[0];
    textarea.innerHTML = "<table>\n";
    for(let i = 1; i <= rows; i++ ){
        textarea.innerHTML = textarea.innerHTML + "&emsp;&lt;tr&gt;\n&emsp;&emsp;";
        for(let j = 1; j <= columns; j++){
            textarea.innerHTML = textarea.innerHTML + `&lt;td&gt;Cell (${i}, ${j})&lt;/td&gt;&nbsp;`;
        }
        textarea.innerHTML = textarea.innerHTML + "\n&emsp;&lt;/tr&gt;\n";
    }
    textarea.innerHTML = textarea.innerHTML + "</table>";
};