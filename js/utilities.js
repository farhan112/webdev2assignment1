//Farhan khandaker 2135266

"use strict";

//Helper method to get an elemant by its ID.
function getId(ID){
    return document.getElementById(ID);
}

//Helper method to get all elements by their tag name.
function getTags(tag){
    return document.getElementsByTagName(tag);
}

//Helper method to create a tag dynamically.
function createTag(tag){
    return document.createElement(tag);
}